### Requirements

* Filling out the template is required. Any Enhancement request that does not include enough information to be reviewed in a timely manner may be closed at the maintainers' discretions.
* All new code requires tests to ensure against regressions

## Description pour cette contribution

<!-- required -->

### Explication pas a pas de cette contribution

<!-- Required -->
1. [First Step]
2. [Second Step]
3. [and so on...]

### Schemas necessaire pour cette contribution

<!-- Describe if schema changes are needed by this contribution -->
<!-- Describe if new schemas are need for this contribution -->

### Benefices

<!-- Required -->
<!-- What benefits will be realized with this contribution ? -->

### Problemes possibles

<!-- optional -->
<!-- What are the possible side-effects or negative impacts of this contribution ? -->

### Cas d'utilisation

<!-- optional -->
<!-- Enter any applicable Issues here -->
