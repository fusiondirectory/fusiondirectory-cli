# \FusionDirectory\Cli

Base for cli tools written in PHP.

The tool base class should extend \FusionDirectory\Cli\Application.
Its constructor should fill $options and $args appropiately.
Its run command should run the tool, it may call parent::run to parse parameters, and parent::runCommand to run subcommands based on $options content.
